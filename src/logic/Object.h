#ifndef OSMIORNICASHOOTER_OBJECT_H
#define OSMIORNICASHOOTER_OBJECT_H

#include <memory>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/vec3.hpp>

class Model;
class GameObjectInitializer;

class Object {
    friend class GameObjectInitializer;
public:
    Object();
    ~Object();

    void setGeometryName(const std::string& name);
    void setTextureName(const std::string& name);

    void translate(const glm::vec3& vector) noexcept;
    void rotate(const glm::vec3& angles) noexcept;
    void scale(float factor) noexcept;

    const glm::mat4& getTransformationMatrix() noexcept;

    Model& getModel() const;

    bool isValid() { return valid; }
    bool isInitialized() { return initialized; }
    bool isUpdated() { return updated; }
private:
    std::unique_ptr<Model> model;

    glm::vec3 position;
    glm::vec3 rotation;
    float scale_factor = 1;
    glm::mat4 modelMatrix;

    // properties
    bool valid = false; /** whether can be displayed properly */
    bool updated = false; // whether needs to be updated
    bool initialized = false; // whether was tried to be initialized
};

#endif //OSMIORNICASHOOTER_OBJECT_H
