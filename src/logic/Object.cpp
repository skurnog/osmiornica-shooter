#include "Object.h"

#include <glm/gtc/matrix_transform.hpp>
#include <resources/model/Model.h>

Object::Object() {
    model = std::make_unique<Model>();
}

Object::~Object() {}

/**
 * Move object in space by a translation vector. After call, object is in outdated state.
 * @param vector - translation vector.
 */
void Object::translate(const glm::vec3 &vector) noexcept {
    position += vector;
    updated = false;
}

/**
 * Add angles to object rotation. After call, object is in outdated state.
 * @param angles - vector of rotation angles around x, y, z axis. In degrees.
 */
void Object::rotate(const glm::vec3& angles) noexcept {
    rotation += glm::radians(angles);
    rotation.x = rotation.x >= 360 ? rotation.x - 360 : rotation.x;
    rotation.y = rotation.y >= 360 ? rotation.y - 360 : rotation.y;
    rotation.z = rotation.z >= 360 ? rotation.z - 360 : rotation.z;
    updated = false;
}

/**
 * Scale object by a factor. After call, object is in outdated state.
 * @param factor - scale factor.
 */
void Object::scale(float factor) noexcept {
    scale_factor *= factor;
    updated = false;
}

/**
 * Calculate and return model matrix basing on current values of transformation, rotation and object scale.
 * When process is completed, object is in **updated** state and subsequent calls of this function will not
 * cause model matrix to be recalculated.
 * @return model matrix.
 */
const glm::mat4 &Object::getTransformationMatrix() noexcept {
    if(!updated) {
        modelMatrix = glm::mat4();
        modelMatrix = glm::scale(modelMatrix, glm::vec3(scale_factor));
        modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1, 0, 0));
        modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0, 1, 0));
        modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0, 0, 1));
        modelMatrix = glm::translate(modelMatrix, position);

        updated = true;
    }
    return modelMatrix;
}

Model &Object::getModel() const {
    return *model;
}

void Object::setGeometryName(const std::string& name) {
    model->setGeometryName(name);
}

void Object::setTextureName(const std::string& name) {
    model->setTextureName(name);
}