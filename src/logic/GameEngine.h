#ifndef OSMIORNICASHOOTER_GAMEENGINE_H
#define OSMIORNICASHOOTER_GAMEENGINE_H

#include <vector>
#include <memory>
#include <glm/vec3.hpp>

class Scene;
class World;

class GameEngine {
public:
    GameEngine();
    ~GameEngine();

    void update() noexcept ;

    void setScene(std::shared_ptr<Scene> scene) noexcept;

    // Input events
    void processKeyEvents(bool *) noexcept;
    void processMouseMoveEvents(double deltaX, double deltaY) noexcept;
    void mouseRightButton(bool press) noexcept;
private:
    const float CAMERA_SPEED = 0.02;

    const float MOUSE_X_SENSITIVITY = 0.1;
    const float MOUSE_Y_SENSITIVITY = 0.1;

    float zSteps = 0;
    float xSteps = 0;

    double mouseDeltaX = 0;
    double mouseDeltaY = 0;

    std::shared_ptr<Scene> scene;
    std::unique_ptr<World> world;

    void resetControlsState() noexcept;

    // GraphicEngine friends
    void cameraUpdate() noexcept ;
    void cameraRotate(double yaw, double pitch) const noexcept;
    void cameraTranslate(glm::vec3 const &vector)const noexcept;
};

#endif //OSMIORNICASHOOTER_GAMEENGINE_H
