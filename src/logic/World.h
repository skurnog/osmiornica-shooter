#ifndef WORLD_H
#define WORLD_H

#include <list>
#include <memory>
#include <vector>

class Object;

class World {
public:
    World();
    ~World();

    std::vector<std::shared_ptr<Object>>& getNewObjects();
    std::vector<std::shared_ptr<Object>> getStaticObjects();

    void updateStaticObjects();
private:
    std::vector<std::shared_ptr<Object>> staticObjects;
    std::vector<std::shared_ptr<Object>> newObjects;
    std::vector<std::shared_ptr<Object>> invalidObjects;

    bool shouldNew;
};

#endif //WORLD_H
