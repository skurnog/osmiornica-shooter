#include "GameEngine.h"

#include <GLFW/glfw3.h>

#include <graphics/Scene.h>
#include <logic/World.h>
#include <logger/loggers.h>

GameEngine::GameEngine(): world(std::make_unique<World>()) {
}

GameEngine::~GameEngine() {
}

void GameEngine::processKeyEvents(bool *keys) noexcept {
    unsigned SPEED_MULTIPLIER = 1;
    if (keys[GLFW_KEY_LEFT_SHIFT]) SPEED_MULTIPLIER = 4;
    if (keys[GLFW_KEY_W]) zSteps += CAMERA_SPEED * SPEED_MULTIPLIER;
    if (keys[GLFW_KEY_S]) zSteps -= CAMERA_SPEED * SPEED_MULTIPLIER;
    if (keys[GLFW_KEY_A]) xSteps += CAMERA_SPEED * SPEED_MULTIPLIER;
    if (keys[GLFW_KEY_D]) xSteps -= CAMERA_SPEED * SPEED_MULTIPLIER;
}

void GameEngine::processMouseMoveEvents(double deltaX, double deltaY) noexcept {
    mouseDeltaX = deltaX * MOUSE_X_SENSITIVITY;
    mouseDeltaY = -deltaY * MOUSE_Y_SENSITIVITY;
}

void GameEngine::mouseRightButton(bool press) noexcept {
}

void GameEngine::resetControlsState() noexcept {
    xSteps = 0;
    zSteps = 0;
}

void GameEngine::cameraTranslate(glm::vec3 const& vector) const noexcept {
    scene->cameraTranslate(vector);
}

void GameEngine::cameraRotate(double pitch, double yaw) const noexcept {
    scene->cameraRotate(pitch, yaw); // todo check if scene valid
}

void GameEngine::cameraUpdate() noexcept {
    cameraTranslate(glm::vec3(xSteps, 0, zSteps));
    cameraRotate(mouseDeltaX, mouseDeltaY);

    resetControlsState();
}

void GameEngine::update() noexcept {
    cameraUpdate();
    world->updateStaticObjects();
    std::vector<std::shared_ptr<Object>> newObjects = world->getNewObjects();
    auto& objectsOnScene = scene->getSceneObjects();
    for(auto it = newObjects.begin(); it != newObjects.end(); ++it) {
        objectsOnScene.push_back(*it);
    }
    scene->update();
}

void GameEngine::setScene(std::shared_ptr<Scene> scene) noexcept {
    this->scene = scene;
}
