#include "World.h"

#include <logic/Object.h>
#include <logger/loggers.h>

World::World(): shouldNew(true) {

}

World::~World() {}

std::vector<std::shared_ptr<Object>> World::getStaticObjects() {
    return staticObjects;
}

void World::updateStaticObjects() {
    newObjects.clear();

    // todo to jest wczytywane z mapManagera czy coś, cały ten if to mock metody
    if(shouldNew) {
        auto obj1 = std::make_shared<Object>();
        obj1->setGeometryName("capsule");
        obj1->setTextureName("capsule");
        obj1->rotate(glm::vec3(-42.6, -55.0, 30));
        obj1->translate(glm::vec3(5.0, 1.0, -7.0));
        newObjects.push_back(obj1);

        auto obj2 = std::make_shared<Object>();
        obj2->setGeometryName("cat");
        obj2->setTextureName("cat");
        obj2->translate(glm::vec3(1.0, 0.0, 0.0));
        obj2->scale(0.3);
        newObjects.push_back(obj2);

        auto obj3 = std::make_shared<Object>();
        obj3->setGeometryName("capsule");
        obj3->setTextureName("capsule");
        obj3->rotate(glm::vec3(-10.0, 30.0, 0));
        obj3->translate(glm::vec3(1.0, 0.0, -1.0));
        newObjects.push_back(obj3);

        auto obj4 = std::make_shared<Object>();
        obj4->setGeometryName("cat");
        obj4->setTextureName("capsule");
        obj4->rotate(glm::vec3(-42.6, -30.0, 30));
        obj4->translate(glm::vec3(1.0, 2.0, 3.0));
        newObjects.push_back(obj4);

        auto obj5 = std::make_shared<Object>();
        obj5->setGeometryName("cat");
        obj5->translate(glm::vec3(2.0, 1.5, 2.2));
        newObjects.push_back(obj5);

        auto obj6 = std::make_shared<Object>();
        obj6->setGeometryName("duracell");
        obj6->setTextureName("duracell");
        obj6->rotate(glm::vec3(-42.6, -40.0, 25));
        obj6->scale(5);
        newObjects.push_back(obj6);

        auto obj7 = std::make_shared<Object>();
        obj7->setGeometryName("plane");
        obj7->setTextureName("plane");
        obj7->translate(glm::vec3(-5.5f, 2.0f, 1.1f));
        obj7->rotate(glm::vec3(180.0f, 0.0f, 0.0f));
        newObjects.push_back(obj7);

        auto obj8 = std::make_shared<Object>();
        obj8->setGeometryName("stalin");
        obj8->scale(0.01);
        obj8->translate(glm::vec3(-1000.5f, 2.0f, 1.1f));
        obj8->rotate(glm::vec3(180.0f, 0.0f, 0.0f));
        newObjects.push_back(obj8);

        auto obj9 = std::make_shared<Object>();
        obj9->setGeometryName("toilet");
        obj9->scale(0.02);
        obj9->translate(glm::vec3(100.5f, -5.0f, 1.1f));
        obj9->rotate(glm::vec3(180.0f, 0.0f, 0.0f));
        newObjects.push_back(obj9);

        auto obj10 = std::make_shared<Object>();
        obj10->setGeometryName("beer_crate");
        obj10->setTextureName("beer_crate");
//        obj10->translate(glm::vec3(5.5f, -5.0f, 2.1f));
        obj10->rotate(glm::vec3(180.0f, 0.0f, 0.0f));
        newObjects.push_back(obj10);

        auto obj11 = std::make_shared<Object>();
        obj11->setGeometryName("eyeball");
        obj11->setTextureName("Eye_D");
        obj11->translate(glm::vec3(-8.5f, -5.0f, 2.1f));
        obj11->rotate(glm::vec3(180.0f, 0.0f, 0.0f));
        newObjects.push_back(obj11);

        shouldNew = false;
    }
}

std::vector<std::shared_ptr<Object>>& World::getNewObjects() {
    return newObjects;
}
