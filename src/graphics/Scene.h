#ifndef OSMIORNICASHOOTER_SCENE_H
#define OSMIORNICASHOOTER_SCENE_H

#include <vector>
#include <memory>
#include <glm/vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>

class Object;
class Camera;
class GameObjectInitializer;

class Scene {
public:
    Scene(unsigned shaderId);
    Scene() = delete;
    ~Scene();

    std::vector<std::shared_ptr<Object>>& getSceneObjects() noexcept;

    void cameraTranslate(const glm::vec3& vector) const noexcept;
    void cameraRotate(double yaw, double pitch) const noexcept;
    const glm::mat4& getCameraMatrix() const noexcept;
    glm::vec3 getCameraPosition() const noexcept;

    void update();
private:
    std::vector<std::shared_ptr<Object>> sceneObjects;
    std::unique_ptr<GameObjectInitializer> gameObjectInitializer;
    std::unique_ptr<Camera> camera;

    bool cached_ld = false;
};

#endif //OSMIORNICASHOOTER_SCENE_H
