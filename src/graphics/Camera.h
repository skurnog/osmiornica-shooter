#ifndef OSMIORNICASHOOTER_CAMERA_H
#define OSMIORNICASHOOTER_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>

class Scene;

class Camera {
    friend class Scene;
public:
    Camera();
    ~Camera() = default;

    const glm::mat4& getTransformation() noexcept;

    void translate(glm::vec3 const& vector) noexcept;
    void rotate(double pitch, double yaw) noexcept;
private:
    glm::vec3 position;

    // Camera rotation
    double pitch = 0;
    double yaw = -90;

    // Camera vector space base
    glm::vec3 cameraUp;
    glm::vec3 cameraFront;
    glm::vec3 cameraRight;

    glm::mat4 transformationMatrix;
};

#endif //OSMIORNICASHOOTER_CAMERA_H
