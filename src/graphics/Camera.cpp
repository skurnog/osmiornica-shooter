#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera() : cameraUp(0.0f, 1.0f, 0.0f), cameraFront(0.0f, 0.0f, -1.0f), cameraRight(1.0f, 0.0f, 0.0f) {
    translate(glm::vec3(0.0f, 0.0f, -2.0f));
}

const glm::mat4& Camera::getTransformation() noexcept {
    transformationMatrix = glm::lookAt(position, position + cameraFront, cameraUp);
    return transformationMatrix;
}

/*
 * Move camera, move target with it.
 */
void Camera::translate(glm::vec3 const& vector) noexcept {
    position = position + cameraRight * vector.x;
    position = position + cameraFront * vector.z;
}

void Camera::rotate(double _yaw, double _pitch) noexcept {
    yaw += _yaw;
    pitch += _pitch;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = static_cast<float>(glm::cos(glm::radians(yaw)) * glm::cos(glm::radians(pitch)));
    front.y = static_cast<float>(glm::sin(glm::radians(pitch)));
    front.z = static_cast<float>(glm::sin(glm::radians(yaw)) * glm::cos(glm::radians(pitch)));

    cameraFront = glm::normalize(front);
    cameraRight = glm::normalize(glm::cross(cameraFront, cameraUp));
}