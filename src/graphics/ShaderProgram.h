#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <glbinding/gl/gl.h>
using namespace gl;

class GraphicsEngine;

class ShaderProgram {
public:
    friend class GraphicsEngine;

    ShaderProgram() = default;
    ~ShaderProgram() = default;

    void init();
    void load_shader(const std::string& file_path, GLenum shader_type);
    void link() const;
    void use() const;
private:
    unsigned program_id;

    unsigned vertex_sh;
    unsigned fragment_sh;

    std::string load_code_from_file(const std::string& file_path) const;
};

#endif //SHADERPROGRAM_H
