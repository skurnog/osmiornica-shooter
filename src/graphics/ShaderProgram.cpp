#include "ShaderProgram.h"

#include <logger/loggers.h>
#include <exceptions/OpenGLException.h>
#include <exceptions/IOException.h>

void ShaderProgram::init()
{
    LOG_GL_ERRORS

    program_id = glCreateProgram();

    if(glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::SHADER);
}

void ShaderProgram::load_shader(const std::string& file_path, GLenum shader_type)
{
    LOG_GL_ERRORS

    unsigned shader_id = glCreateShader(shader_type);
    if(glGetError() == GL_INVALID_ENUM) throw ex::OpenGLException(ErrorType::SHADER, "Invalid type");
    if(shader_id == 0) throw ex::OpenGLException(ErrorType::SHADER, "0 returned from glCreateShader");

    if(shader_type == GL_VERTEX_SHADER) {
        vertex_sh = shader_id;
    } else if (shader_type == GL_FRAGMENT_SHADER) {
        fragment_sh = shader_id;
    }

    std::string shader_src_str = load_code_from_file(file_path);
    const char* shader_src = shader_src_str.c_str();

    glShaderSource(shader_id, 1, &shader_src, nullptr);

    int success;
    glCompileShader(shader_id);
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
    if(!success) {
        char infolog[512];
        glGetShaderInfoLog(shader_id, 512, nullptr, infolog);
        throw ex::OpenGLException(ErrorType::SHADER, infolog);
    }

    glAttachShader(program_id, shader_id);

    if(glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::SHADER);
    else LOG_INFO << "Shader compiled and attached: " << shader_type;
}

void ShaderProgram::link() const
{
    LOG_GL_ERRORS

    int success;
    glLinkProgram(program_id);
    glGetProgramiv(program_id, GL_LINK_STATUS, &success);
    if(!success) {
        char infolog[512];
        glGetProgramInfoLog(program_id, 512, nullptr, infolog);
        throw ex::OpenGLException(ErrorType::SHADER, infolog);
    }

    glDetachShader(program_id, vertex_sh);
    glDeleteShader(vertex_sh);
    glDetachShader(program_id, fragment_sh);
    glDeleteShader(fragment_sh);

    if(glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::SHADER, "shader program link");
    else LOG_INFO << "Shader program linked";
}

void ShaderProgram::use() const
{
    LOG_GL_ERRORS

    glUseProgram(program_id);

    if(glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::SHADER, "shader program use");
}

std::string ShaderProgram::load_code_from_file(const std::string& file_path) const
{
    std::fstream file;
    file.exceptions(std::ios_base::failbit | std::ios_base::badbit);

    try {
        file.open(file_path, std::ios::in);
    } catch(const std::exception& e) {
        throw ex::IOException(e, file_path);
    }

    try {
        std::stringstream ss;
        ss << file.rdbuf();
        std::string shader_code = ss.str();

        return shader_code;
    } catch (const std::exception& e) {
        throw ex::IOException(e, file_path);
    }
}
