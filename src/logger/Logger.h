#ifndef LOGGER_H
#define LOGGER_H

#include "LogStream.h"

/***
 * Logger frontend. Enables usage of stream-like syntax for logging.
 */
class Logger
{
public:
    explicit Logger (LogStream & logger, ErrorType msg_type) : _logger(logger)
    {
        _logger.start(msg_type);
    }
    ~Logger () { _logger.finish(); }

    LogStream& logger() const {return _logger;}

private:
    LogStream& _logger;
};

template <typename T>
Logger const & operator<< (Logger const & ae, T && arg)
{
    ae.logger() << std::forward<T>(arg);
    return ae;
}

#endif //LOGGER_H
