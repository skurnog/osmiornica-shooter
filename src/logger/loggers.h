#ifndef LOGGERS_H
#define LOGGERS_H

#include "LogStream.h"
#include "Logger.h"

#undef __gl_h_
#include <glbinding/gl/types.h>
#define __gl_h_

#include <chrono>

#ifdef N_LOG_CONSOLE
#   define CONSOLE_LOG false
#else
#   define CONSOLE_LOG true
#endif

#ifdef N_LOG_FILE
#   define FILE_LOG false
#else
#   define FILE_LOG true
#endif

#ifdef N_LOG_ERROR
#   define CONSOLE_LOG_ERR false
#   define FILE_LOG_ERR false
#else
#   define CONSOLE_LOG_ERR CONSOLE_LOG
#   define FILE_LOG_ERR FILE_LOG
#endif
static LogStream log_err_stream(LogLevel::ERROR, CONSOLE_LOG_ERR, FILE_LOG_ERR);
#define _LOG_ERROR(msg_type) Logger(log_err_stream, msg_type)
#define LOG_ERROR(msg_type) _LOG_ERROR(msg_type) << "(file: " << __FILE__ << ", line: " << __LINE__ << "): "

#ifdef N_LOG_INFO
#   define CONSOLE_LOG_INF false
#   define FILE_LOG_INF false
#else
#   define CONSOLE_LOG_INF CONSOLE_LOG
#   define FILE_LOG_INF FILE_LOG
#endif
static LogStream log_inf_stream(LogLevel::INFO, CONSOLE_LOG_INF, FILE_LOG_INF);
#define LOG_INFO Logger(log_inf_stream, ErrorType::DEFAULT)

#ifdef N_LOG_DEBUG
#   define CONSOLE_LOG_DBG false
#   define FILE_LOG_DBG false
#else
#   define CONSOLE_LOG_DBG CONSOLE_LOG
#   define FILE_LOG_DBG FILE_LOG
#endif
static LogStream log_dbg_stream(LogLevel::DEBUG, CONSOLE_LOG_DBG, FILE_LOG_DBG);
#define _LOG_DEBUG Logger(log_dbg_stream, ErrorType::DEFAULT)
#define LOG_DEBUG _LOG_DEBUG << "(file: " << __FILE__ << ", line: " << __LINE__ << ")  "

#undef CONSOLE_LOG
#undef FILE_LOG

// useful macros
#define LOG_GL_ERRORS GLenum glErrorCode; glGetError();
#define IS_GL_ERROR (glErrorCode = glGetError()) != GL_NO_ERROR
#define GL_ERROR_STRING [glErrorCode]() -> const std::string {std::stringstream ss; ss << glErrorCode; return ss.str();}()

#define MEASURE_TIME auto start_time = std::chrono::high_resolution_clock::now();
#define TIME_ELAPSED [&start_time](){return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time).count();}()

#endif //LOGGERS_H
