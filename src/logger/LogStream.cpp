#include "LogStream.h"

// todo error severity, remove type

LogStream::LogStream(LogLevel logLevel, bool log_console, bool log_file):
        level(logLevel), log_console(log_console), log_file(log_file)
{
    std::string file_name = "~/os.log"; // TODO: file logging!
    file_out.exceptions();
    try {
        file_out.open(file_name, std::ios::out | std::ios::app);
    } catch (std::exception& e) {
        std::cout << "[ERROR] Cannot open file for log! " << file_name << std::endl << std::flush;
        log_file = false;
    }

    time_buffer = std::unique_ptr<char[]>(new char[50]);
}

void LogStream::start(ErrorType type)
{
    buffer.clear();
    buffer.str("");

    update_time();
    buffer << time_buffer.get() << levels.at(level) << errorDescriptions.at(type) << " ";
}

void LogStream::finish()
{
    if(log_console) std::cout << buffer.str() << std::endl << std::flush;
    if(log_file) file_out << buffer.str() << std::endl << std::flush;
}

void LogStream::update_time()
{
    std::time(&raw_time);
    time_info = std::localtime(&raw_time);
    std::strftime(time_buffer.get(), 50, "%Y/%m/%d %H:%M:%S ", time_info);
}