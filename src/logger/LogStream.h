#ifndef LOGSTREAM_H
#define LOGSTREAM_H

#include <sstream>
#include <iostream>
#include <fstream>

#include <map>
#include <memory>

#include <ctime>

enum class LogLevel
{
    DEBUG, ERROR, INFO
};

enum class ErrorType
{
    DEFAULT, GLFW, GEN, BIND, SHADER, VERTEX_ATTRIB, BUFFER_DATA, FILE, SHADER_VAR_LOCATION, SHADER_MATRIX,
    MODEL_LOAD, TEXTURE_LOAD,
    OBJ_PARSER_ERROR, FRAME_RENDER,
    GAME_ENGINE_INVALID, PROGRAM_ARGUMENT
};

const std::map<ErrorType, std::string> errorDescriptions = {
        {ErrorType::DEFAULT, ""},
        {ErrorType::GLFW, "Problem reported by glfw:"},
        {ErrorType::GEN, "OpenGL object (VAO/VBO) generating error."},
        {ErrorType::BIND, "Problem when binding OpenGL object:"},
        {ErrorType::VERTEX_ATTRIB, "Problem when processing vertex attribute"},
        {ErrorType::BUFFER_DATA, "Problem when loading data to VRAM:"},
        {ErrorType::SHADER, "Problem with shader ocurred"},
        {ErrorType::FILE, "Problem with file:"},
        {ErrorType::OBJ_PARSER_ERROR, "File is not a valid obj!"},
        {ErrorType::SHADER_VAR_LOCATION, "Unable to locate shader variable:"},
        {ErrorType::SHADER_MATRIX, "Problem with function glUniformMatrix for: "},
        {ErrorType::MODEL_LOAD, "Problem with loading model occured"},
        {ErrorType::FRAME_RENDER, "Problem when rendering frame."},
        {ErrorType::TEXTURE_LOAD, "Problem when loading texture."},
        {ErrorType::GAME_ENGINE_INVALID, "Game engine did not initialize properly."},
        {ErrorType::PROGRAM_ARGUMENT, "Program argument not recognized."}
};

/***
 * Logger backend - builds the message, writes to output.
 */
class LogStream {
public:
    LogStream(LogLevel logLevel, bool log_console, bool log_file);

    template <typename T> LogStream & operator<<(const T& msg)
    {
        buffer << msg;
        return *this;
    }

    void start(ErrorType type);
    void finish();

private:
    std::ostringstream buffer;

    LogLevel level;
    bool log_console;
    std::ofstream file_out;
    bool log_file;

    std::time_t raw_time;
    std::tm* time_info;
    std::unique_ptr<char[]> time_buffer;

    void update_time();

    const std::map<LogLevel, std::string> levels = {
            {LogLevel::DEBUG, "[DEBUG]"},
            {LogLevel::ERROR, "[ERROR] "},
            {LogLevel::INFO, "[INFO]"}
    };
};

#endif //LOGSTREAM_H
