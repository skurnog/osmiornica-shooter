#include "GameObjectInitializer.h"

#include <logic/Object.h>
#include <resources/ResourceManager.h>
#include <resources/model/Model.h>
#include <resources/model/Geometry.h>
#include <exceptions/Exception.h>

#include <algorithm>

GameObjectInitializer::GameObjectInitializer(unsigned shaderId) {
    resourceManager = std::make_unique<ResourceManager>();
    resourceManager->setShaderId(shaderId);
}

GameObjectInitializer::~GameObjectInitializer() {

}

void GameObjectInitializer::initialize(Object& gameObject) {
    if(gameObject.isInitialized()) return;

    bool valid = true;

    Model& model = gameObject.getModel();

    try {
        if(!model.geometry_name.empty()) {
            model.geometry = resourceManager->getGeometry(model.geometry_name);
        }
    } catch (ex::Exception& ex) {
        LOG_ERROR(ErrorType::MODEL_LOAD) << model.geometry_name << "\n" << ex.what();
        valid = false;
    }

    try {
        if(!model.texture_name.empty()) {
            model.texture = resourceManager->getTexture(model.texture_name);
        } else {
            model.texture = resourceManager->getTexture("default");
        }
    } catch (ex::Exception& e) {
        LOG_ERROR(ErrorType::TEXTURE_LOAD) << model.texture_name << "\n" << e.what();
        valid = false;
    }

    gameObject.initialized = true;
    gameObject.valid = valid;
}

void GameObjectInitializer::initialize(std::vector<std::shared_ptr<Object>> &gameObjects) {
    std::vector<std::string> model_names;

    for(auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
        auto geometry_name = (*it)->getModel().getGeometryName();
        if(std::find(model_names.begin(), model_names.end(), geometry_name) == model_names.end())
            model_names.emplace_back(geometry_name); // make them unique
    }

    resourceManager->loadGeometries(model_names);

    for(auto& uninitialized_object: gameObjects) {
        initialize(*uninitialized_object);
    }
}

void GameObjectInitializer::updateCache(const std::vector<std::string>& models) {
    resourceManager->loadGeometries(models);
}