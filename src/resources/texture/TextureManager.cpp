#include "TextureManager.h"

#include <glbinding/gl/gl.h>
using namespace gl;

#include <resources/texture/Texture.h>
#include <resources/AssetLoader.h>
#include <logger/loggers.h>
#include <exceptions/OpenGLException.h>

TextureManager::TextureManager(std::shared_ptr<AssetLoader> assetLoader):
        assetLoader(assetLoader) {
}

TextureManager::~TextureManager() {
}

std::shared_ptr<Texture> TextureManager::load(const std::string& name) {
    this->texture = std::make_shared<Texture>(name);
    try {
        loadFile(texturePath(name));
    } catch (ex::Exception& e) {
        loadFile(texturePath("default"));
        LOG_ERROR(ErrorType::TEXTURE_LOAD) << name << "\n" << e.what() << "\nSwitched to default.";
    }

    return std::move(texture);
}

void TextureManager::loadFile(const std::string& file_name) {
    assetLoader->load_texture(file_name, *texture);

    LOG_GL_ERRORS
    glGenTextures(1, &texture->texture_id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, static_cast<int>(GL_RGBA), texture->texture_width, texture->texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &texture->texture_image[0]);
    glBindTexture(GL_TEXTURE_2D, 0);

    if(glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::TEXTURE_LOAD);
    else LOG_INFO << "Texture loaded: " << texture->name;
}

std::string TextureManager::texturePath(const std::string& model_name) const noexcept {
    return TEXTURE_DIR + model_name + TEXTURE_EXT;
}