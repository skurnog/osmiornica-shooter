#ifndef OSMIORNICASHOOTER_TEXTURE_H
#define OSMIORNICASHOOTER_TEXTURE_H

#include <memory>

class AssetLoader;
class TextureManager;
class Model;

class Texture {
    friend class AssetLoader;
    friend class TextureManager;
    friend class Model;
public:
    Texture(const std::string& name);
    ~Texture();

    unsigned getTextureId() { return texture_id; }
private:
    std::string name;

    unsigned texture_id;
    int texture_width;
    int texture_height;

    std::unique_ptr<unsigned char[]> texture_image;
};

#endif //OSMIORNICASHOOTER_TEXTURE_H
