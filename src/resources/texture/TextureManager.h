#ifndef OSMIORNICASHOOTER_TEXTURELOADER_H
#define OSMIORNICASHOOTER_TEXTURELOADER_H

#include <string>
#include <memory>

class Texture;
class AssetLoader;

class TextureManager {
public:
    TextureManager(std::shared_ptr<AssetLoader> assetLoader);
    ~TextureManager();

    std::shared_ptr<Texture> load(const std::string& name);
private:
    const std::string CORE_DIR = "../resources/";
    const std::string TEXTURE_DIR = CORE_DIR + "textures/";
    const std::string TEXTURE_EXT = ".png";

    std::shared_ptr<AssetLoader> assetLoader;
    std::shared_ptr<Texture> texture;

    void loadFile(const std::string& file_name);

    std::string texturePath(const std::string& model_name) const noexcept;
};

#endif //OSMIORNICASHOOTER_TEXTURELOADER_H
