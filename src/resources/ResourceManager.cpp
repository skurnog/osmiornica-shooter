#include "ResourceManager.h"

#include <resources/AssetLoader.h>
#include <resources/model/GeometryManager.h>
#include <resources/model/Geometry.h>
#include <resources/model/Mesh.h>
#include <resources/texture/TextureManager.h>
#include <algorithm>
#include <logger/loggers.h>

ResourceManager::ResourceManager() {
    auto assetLoader = std::make_shared<AssetLoader>();

    geometryManager = std::make_unique<GeometryManager>(assetLoader);
    textureManager = std::make_unique<TextureManager>(assetLoader);
}

ResourceManager::~ResourceManager() {
}

void ResourceManager::loadGeometries(const std::vector<std::string>& name_vec) {
    auto not_cached_yet = name_vec;
    not_cached_yet.erase(std::remove_if(not_cached_yet.begin(), not_cached_yet.end(), [this](std::string& name){
        return this->geometryCache.count(name) > 0;
    }), not_cached_yet.end());

    auto geometries = geometryManager->load(not_cached_yet);
    for(auto& geometry: geometries) {
        auto name = geometry->getName();
        geometryCache[name] = std::shared_ptr<Geometry>(std::move(geometry));
    }
}

std::shared_ptr<Geometry> ResourceManager::getGeometry(const std::string& name) {
    try {
        return geometryCache.at(name);
    } catch (const std::out_of_range& ex) {
        auto geometry = geometryManager->load(name);
        geometryCache[name] = std::shared_ptr<Geometry>(std::move(geometry));

        return geometryCache.at(name);
    }
}

std::shared_ptr<Texture> ResourceManager::getTexture(const std::string& name) {
    try {
        return textureCache.at(name);
    } catch (const std::out_of_range& ex) {
        auto texture = textureManager->load(name);
        textureCache[name] = std::shared_ptr<Texture>(std::move(texture));

        return textureCache.at(name);
    }
}

void ResourceManager::setShaderId(unsigned shaderId) noexcept {
    geometryManager->shaderId = shaderId;
}
