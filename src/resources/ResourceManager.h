#ifndef OSMIORNICASHOOTER_RESOURCEMANAGER_H
#define OSMIORNICASHOOTER_RESOURCEMANAGER_H

#include <memory>
#include <map>
#include <vector>

class Geometry;
class GeometryManager;
class Texture;
class TextureManager;

class ResourceManager {
public:
    ResourceManager();
    ~ResourceManager();

    void loadGeometries(const std::vector<std::string>& name_vec);

    std::shared_ptr<Geometry> getGeometry(const std::string& name);
    std::shared_ptr<Texture> getTexture(const std::string& name);
    void setShaderId(unsigned shaderId) noexcept;
private:
    std::map<std::string, std::shared_ptr<Geometry>> geometryCache;
    std::map<std::string, std::shared_ptr<Texture>> textureCache;

    std::unique_ptr<GeometryManager> geometryManager;
    std::unique_ptr<TextureManager> textureManager;
};

#endif //OSMIORNICASHOOTER_RESOURCEMANAGER_H
