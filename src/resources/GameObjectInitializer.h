#ifndef OSMIORNICASHOOTER_GAMEOBJECTINITIALIZER_H
#define OSMIORNICASHOOTER_GAMEOBJECTINITIALIZER_H

#include <memory>
#include <vector>

class Object;
class ResourceManager;

class GameObjectInitializer {
    friend class Object;
public:
    GameObjectInitializer(unsigned shaderId);
    ~GameObjectInitializer();

    void initialize(Object& gameObject);
    void initialize(std::vector<std::shared_ptr<Object>>& gameObjects);
    void updateCache(const std::vector<std::string>& models);
private:
    std::unique_ptr<ResourceManager> resourceManager;
};

#endif //OSMIORNICASHOOTER_GAMEOBJECTINITIALIZER_H
