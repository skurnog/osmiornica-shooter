#include "AssetLoader.h"

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/mesh.h>
#include <assimp/scene.h>

#include <SOIL.h>

#include <vector>
#include <memory>

#include <resources/model/Geometry.h>
#include <resources/model/Mesh.h>
#include <resources/texture/Texture.h>
#include <logger/loggers.h>
#include <exceptions/ResourceManagerException.h>

std::unique_ptr<Geometry> AssetLoader::load_geometry(const std::string& file_path, const std::string& model_name) {
    std::unique_ptr<aiScene> scene(
            (aiScene *) aiImportFile(file_path.c_str(), aiProcess_Triangulate | aiProcess_JoinIdenticalVertices));

    if (scene.get() == nullptr)
        throw ex::ResourceManagerException(ErrorType::MODEL_LOAD, "file: " + file_path + "\n" + aiGetErrorString());

    std::unique_ptr<Geometry> geometry = std::make_unique<Geometry>(model_name);

    for (int mesh_num = 0; mesh_num < (int) (*scene).mNumMeshes; mesh_num++) {
        aiMesh *ai_mesh = (*scene).mMeshes[mesh_num];

        geometry->getMeshes().emplace_back();
        Mesh& mesh = geometry->getMeshes().back();
        mesh.textured = ai_mesh->HasTextureCoords(0);
        mesh.has_normals = ai_mesh->HasNormals();

        mesh.vertices_count = ai_mesh->mNumVertices;

        for (int face_num = 0; face_num < (int) ai_mesh->mNumFaces; ++face_num) {
            mesh.indices.push_back(ai_mesh->mFaces[face_num].mIndices[0]);
            mesh.indices.push_back(ai_mesh->mFaces[face_num].mIndices[1]);
            mesh.indices.push_back(ai_mesh->mFaces[face_num].mIndices[2]);
        }

        for (int vert_num = 0; vert_num < (int) ai_mesh->mNumVertices; ++vert_num) {
            mesh.vertices.push_back(ai_mesh->mVertices[vert_num].x);
            mesh.vertices.push_back(ai_mesh->mVertices[vert_num].y);
            mesh.vertices.push_back(ai_mesh->mVertices[vert_num].z);
        }

        if (mesh.textured) {
            for (int texcoord_num = 0; texcoord_num < (int) ai_mesh->mNumVertices; ++texcoord_num) {
                mesh.texture_coords.push_back(ai_mesh->mTextureCoords[0][texcoord_num].x);
                mesh.texture_coords.push_back(ai_mesh->mTextureCoords[0][texcoord_num].y);
            }
        }

        if (mesh.has_normals) {
            for (int normal_num = 0; normal_num < (int) ai_mesh->mNumVertices; ++normal_num) {
                mesh.normals.push_back(ai_mesh->mNormals[normal_num].x);
                mesh.normals.push_back(ai_mesh->mNormals[normal_num].y);
                mesh.normals.push_back(ai_mesh->mNormals[normal_num].z);
            }
        }

        mesh.packed_vertex_data = pack_vertex_data(mesh);
    }

    return std::move(geometry);
}

std::vector<float> AssetLoader::pack_vertex_data(const Mesh& mesh) {
    std::vector<float> vertex_data;
    try {
        for (unsigned i = 0; i < mesh.vertices_count; i++) {
            vertex_data.push_back(mesh.vertices.at(i * 3));
            vertex_data.push_back(mesh.vertices.at(i * 3 + 1));
            vertex_data.push_back(mesh.vertices.at(i * 3 + 2));
            if(mesh.textured) {
                vertex_data.push_back(mesh.texture_coords.at(i * 2));
                vertex_data.push_back(mesh.texture_coords.at(i * 2 + 1));
            }
            if(mesh.has_normals) {
                vertex_data.push_back(mesh.normals.at(i * 3));
                vertex_data.push_back(mesh.normals.at(i * 3 + 1));
                vertex_data.push_back(mesh.normals.at(i * 3) + 2);
            }
        }
    } catch (std::exception& ex) {
        throw ex::ResourceManagerException(ex, "pack_vertex_data");
    }

    return vertex_data;
}

void AssetLoader::load_texture(const std::string& file_path, Texture& texture) {
    texture.texture_image = std::unique_ptr<unsigned char[]>(SOIL_load_image(file_path.c_str(), &texture.texture_width, &texture.texture_height, 0, SOIL_LOAD_RGBA));

    if (texture.texture_image == 0) {
        throw ex::ResourceManagerException(ErrorType::TEXTURE_LOAD,"Error when loading texture file.");
    }
}

