#include "Model.h"

#include <resources/lighting/Lighting.h>

Model::Model(): ambient_intensity(DEFAULT_AMBIENT_INTENSITY) {}

Model::~Model() {
}

void Model::setTextureName(const std::string &name) {
    texture_name = name;
}

void Model::setGeometryName(const std::string &name) {
    geometry_name = name;
}

bool Model::geometrySet() {
    return geometry != nullptr;
}

bool Model::textureSet() {
    return texture != nullptr;
}

float Model::getAmbientIntensity() {
    return ambient_intensity;
}