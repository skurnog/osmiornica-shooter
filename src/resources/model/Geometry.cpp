#include "Geometry.h"

#include <algorithm>

#include <resources/model/Mesh.h>

Geometry::Geometry(const std::string& name): name(name) {
}

unsigned long Geometry::getOverallElementsCount() noexcept {
    if(!elements_count_valid) {
        elements_count = 0;
        std::for_each(meshes.begin(), meshes.end(), [this](Mesh& mesh) {
            elements_count += mesh.getElementsCount();
        });
        elements_count_valid = true;
    }
    return elements_count;
}