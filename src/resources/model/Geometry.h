#ifndef OSMIORNICASHOOTER_GEOMETRY_H
#define OSMIORNICASHOOTER_GEOMETRY_H

#include <string>
#include <vector>

class Mesh;
class Model;

class Geometry {
    friend class Model;
public:
    Geometry(const std::string& name);
    const std::string& getName() const noexcept { return name; }
    unsigned long getOverallElementsCount() noexcept;

    std::vector<Mesh>& getMeshes() { return meshes; };
private:
    std::string name;
    std::vector<Mesh> meshes;

    unsigned long elements_count;
    bool elements_count_valid = false;
};

#endif //OSMIORNICASHOOTER_GEOMETRY_H
