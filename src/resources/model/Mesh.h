#ifndef OSMIORNICASHOOTER_MESH_H
#define OSMIORNICASHOOTER_MESH_H

#include <vector>

class AssetLoader;
class GeometryManager;

class Mesh {
    friend class AssetLoader;
    friend class GeometryManager;
public:
    unsigned long getElementsCount() const noexcept { return indices.size(); }
    unsigned getVAO() const noexcept { return VAO; }
private:
    std::vector<float> vertices;
    std::vector<unsigned> indices;
    std::vector<float> texture_coords;
    std::vector<float> normals;

    std::vector<float> packed_vertex_data;

    unsigned vertices_count;

    unsigned VAO;
    unsigned VBO;
    unsigned EBO;

    bool textured = false;
    bool has_normals = false;
};

#endif //OSMIORNICASHOOTER_MESH_H
