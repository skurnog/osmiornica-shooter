#include "GeometryManager.h"

#include <glbinding/gl/gl.h>
using namespace gl;

#include <future>

#include <resources/AssetLoader.h>
#include <resources/model/Geometry.h>
#include <resources/model/Mesh.h>
#include <exceptions/ResourceManagerException.h>
#include <exceptions/OpenGLException.h>

GeometryManager::GeometryManager(std::shared_ptr<AssetLoader> assetLoader):
        assetLoader(assetLoader) {
}

GeometryManager::~GeometryManager() {
}

/**
 * Loads multiple models asynchronically.
 * @param names_vec list of names of models to be loaded.
 * @return vector of initialized Geometry objects.
 */
std::vector<std::unique_ptr<Geometry>> GeometryManager::load(const std::vector<std::string>& names_vec) {
    MEASURE_TIME
    LOG_INFO << "Loading models...";

    unsigned all_models_count = names_vec.size();
    unsigned loaded_models_count = 0;

    std::vector<std::unique_ptr<Geometry>> result;
    std::vector<std::future<std::unique_ptr<Geometry>>> futures;
    for(auto& name: names_vec) {
        futures.push_back(std::move(std::async(&GeometryManager::loadModelFile, this, name))); // todo exceptions
    }

    for(auto& future: futures) {
        future.wait();
        auto geometry = future.get();

        generateVAOs(geometry, shaderId);
        result.push_back(std::move(geometry));

        loaded_models_count++;
        LOG_INFO << "Loaded *" << result.back()->getName() << "* [" << loaded_models_count << "/" << all_models_count << "]...";
    }

    LOG_INFO << "Models loaded [" << TIME_ELAPSED << "ms]";
    return std::move(result);
}

std::unique_ptr<Geometry> GeometryManager::load(const std::string &name) {
    std::unique_ptr<Geometry> geometry = loadModelFile(name);
    generateVAOs(geometry, shaderId);

    return std::move(geometry);
}

std::string GeometryManager::model_path(const std::string& model_name) const noexcept {
    return MODEL_DIR + model_name + MODEL_EXT;
}

std::unique_ptr<Geometry> GeometryManager::loadModelFile(const std::string& model_name) {
    return assetLoader->load_geometry(model_path(model_name), model_name);
}

void GeometryManager::generateVAOs(std::unique_ptr<Geometry>& geometry, unsigned shaderId) {
    this->shaderId = shaderId;
    for(Mesh& mesh: geometry->getMeshes()) {
        generateVAO(mesh); // todo sprawdzic czy model zaladowany, wyjatek etc.
    }
}

void GeometryManager::generateVAO(Mesh& mesh) {
    try {
        LOG_GL_ERRORS

        glGenVertexArrays(1, &mesh.VAO);
        glBindVertexArray(mesh.VAO);

        glGenBuffers(1, &mesh.VBO);
        glGenBuffers(1, &mesh.EBO);

        if (glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::GEN);

        unsigned stride = 3;
        if(mesh.textured) stride += 2;
        if(mesh.has_normals) stride += 3;

        // Load data to buffer object
        glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * mesh.packed_vertex_data.size(), mesh.packed_vertex_data.data(), GL_STATIC_DRAW);

        // Position attribute
        int sh_attr_position = glGetAttribLocation(shaderId, "posAttr");
        glVertexAttribPointer((GLuint) sh_attr_position, 3, GL_FLOAT, GL_TRUE, stride * sizeof(GLfloat), (void *) 0);
        glEnableVertexAttribArray((GLuint) sh_attr_position);

        if (IS_GL_ERROR) throw ex::OpenGLException(ErrorType::VERTEX_ATTRIB, "posAttr", GL_ERROR_STRING);

        // Texture
        if(mesh.textured) {
            int sh_tex_coord = glGetAttribLocation(shaderId, "texCoord");
            glVertexAttribPointer((GLuint) sh_tex_coord, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void *) (3 * sizeof(float)));
            glEnableVertexAttribArray((GLuint) sh_tex_coord);

            if(IS_GL_ERROR) throw ex::OpenGLException(ErrorType::VERTEX_ATTRIB, "texCoord", GL_ERROR_STRING);
        }

        // normal vectors
        if(mesh.has_normals) {
            int sh_attr_normal = glGetAttribLocation(shaderId, "normalVecAttr");
            glVertexAttribPointer((GLuint) sh_attr_normal, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*) (5 * sizeof(float))); // todo ten ostatni argument...
            glEnableVertexAttribArray((GLuint) sh_attr_normal);

            if(IS_GL_ERROR) throw ex::OpenGLException(ErrorType::VERTEX_ATTRIB, "normalVecAttr", GL_ERROR_STRING);
        }

        // Load data to index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * mesh.indices.size(), mesh.indices.data(),
                     GL_STATIC_DRAW);

        glBindVertexArray(0);

        if (IS_GL_ERROR) throw ex::OpenGLException(ErrorType::MODEL_LOAD, GL_ERROR_STRING);
    } catch (ex::Exception& ex) {
        glBindVertexArray(0);

        if(typeid(ex) == typeid(ex::OpenGLException)) throw;
        throw ex::OpenGLException(ex);
    }
}

