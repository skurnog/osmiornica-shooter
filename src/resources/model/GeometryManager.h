#ifndef OSMIORNICASHOOTER_MODELLOADER_H
#define OSMIORNICASHOOTER_MODELLOADER_H

#include <memory>
#include <vector>

class Geometry;
class AssetLoader;
class Mesh;

class GeometryManager {
    friend class ResourceManager;
public:
    GeometryManager(std::shared_ptr<AssetLoader> assetLoader);
    ~GeometryManager();

    std::unique_ptr<Geometry> load(const std::string& name);
    std::vector<std::unique_ptr<Geometry>> load(const std::vector<std::string>& names_vec);
private:
    const std::string CORE_DIR = "../resources/";
    const std::string MODEL_DIR = CORE_DIR + "models/";
    const std::string MODEL_EXT = ".obj";

    unsigned int shaderId;
    std::shared_ptr<AssetLoader> assetLoader;

    std::unique_ptr<Geometry> loadModelFile(const std::string& model_name);
    void generateVAOs(std::unique_ptr<Geometry>&, unsigned shaderId);
    void generateVAO(Mesh&);

    std::string model_path(const std::string& model_name) const noexcept;
};

#endif //OSMIORNICASHOOTER_MODELLOADER_H
