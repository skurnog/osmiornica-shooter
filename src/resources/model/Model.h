#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <vector>
#include <memory>

class Geometry;
class Texture;
class GameObjectInitializer;

class Model {
    friend class GameObjectInitializer;
public:
    Model();
    ~Model();

    void setGeometryName(const std::string& name);
    void setTextureName(const std::string& name);

    const std::string& getGeometryName() { return geometry_name; }
    const std::string& getTextureName() { return texture_name; }

    bool geometrySet();
    bool textureSet();

    Geometry& getGeometry() { return *geometry; }
    Texture& getTexture() { return *texture; }

    float getAmbientIntensity();
private:
    std::string model_name;

    std::shared_ptr<Geometry> geometry;
    std::shared_ptr<Texture> texture;

    std::string geometry_name;
    std::string texture_name;

    // light // todo move to Lightning class when and IF necessary
    float ambient_intensity;
};

#endif //MODEL_H
