#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <string>
#include <vector>
#include <bits/unique_ptr.h>

class Geometry;
class Texture;
class Mesh;

class AssetLoader {
public:
    AssetLoader() = default;
    ~AssetLoader() = default;

    std::unique_ptr<Geometry> load_geometry(const std::string& file_path, const std::string& model_name);
    void load_texture(const std::string &file_path, Texture &texture);

private:
    std::vector<float> pack_vertex_data(const Mesh& mesh);
};

#endif //OBJLOADER_H
