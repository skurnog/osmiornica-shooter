#include "Exception.h"

#include <sstream>

namespace ex {

    Exception::Exception(const std::string& cause): cause(cause) {}

    Exception::Exception(const std::string& cause, const std::exception& original_ex, const std::string& msg):
            cause(cause), original_exception_what(original_ex.what()), msg(msg) {
    }

    Exception::Exception(const std::string& cause, const std::string& msg): cause(cause), msg(std::move(msg)) {
    }

    std::string Exception::what() {
        std::stringstream ss;
        ss << "Caused by -> " << cause;

        if(!msg.empty()) ss << std::endl << "Supplied message -> " << msg;
        if(!original_exception_what.empty()) ss << std::endl << "Original exception what() -> " << original_exception_what;

        return ss.str();
    }
}