#ifndef OSMIORNICASHOOTER_IOEXCEPTION_H
#define OSMIORNICASHOOTER_IOEXCEPTION_H

#include "Exception.h"

namespace ex {

    class IOException : public Exception {
    public:
        IOException(): Exception("IOException") {}
        IOException(const std::string& msg): Exception("IOException", msg) {};
        IOException(const std::exception& original_ex, const std::string& msg = ""): Exception("IOException", original_ex, msg) {}
        IOException(const Exception& exception): Exception(exception) { cause = "IOException->" + cause; }
        IOException(const ErrorType& errorType): Exception(errorDescriptions.at(errorType)) {};
        IOException(const ErrorType& errorType, const std::string& info): Exception(errorDescriptions.at(errorType), info) {}
    };

}

#endif //OSMIORNICASHOOTER_IOEXCEPTION_H
