#ifndef OSMIORNICASHOOTER_OPENGLEXCEPTION_H
#define OSMIORNICASHOOTER_OPENGLEXCEPTION_H

#include "Exception.h"

extern const std::map<ErrorType, std::string> errorDescriptions;

namespace ex {

    class OpenGLException : public Exception {
    public:
        OpenGLException(): Exception("OpenGLException") {}
        OpenGLException(const std::string& msg): Exception("OpenGLException", msg) {}
        OpenGLException(const std::exception& original_ex, const std::string& msg = ""): Exception("OpenGLException", original_ex, msg) {}
        OpenGLException(const Exception& exception): Exception(exception) { cause = "OpenGLException->" + cause; }
        OpenGLException(const ErrorType& errorType): Exception(errorDescriptions.at(errorType)) {};
        OpenGLException(const ErrorType& errorType, const std::string& info): Exception(errorDescriptions.at(errorType), info) {}
        OpenGLException(const ErrorType& errorType, const std::string& additionalTypeInfo, const std::string& info):
                Exception(errorDescriptions.at(errorType) + ": " + additionalTypeInfo, info) {}
    };

}

#endif //OSMIORNICASHOOTER_OPENGLEXCEPTION_H
