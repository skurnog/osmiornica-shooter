#ifndef OSMIORNICASHOOTER_EXCEPTION_H
#define OSMIORNICASHOOTER_EXCEPTION_H

#include <string>
#include <map>
#include <logger/loggers.h>

namespace ex {

    class Exception {
    public:
        std::string what();

        Exception(const std::string& cause);
        Exception(const std::string& cause, const std::exception& original_ex, const std::string& msg = "");
        Exception(const std::string& cause, const std::string& msg);

        virtual ~Exception() {};
    protected:
        std::string cause;
    private:
        std::string original_exception_what;
        std::string msg;
    };

}

#endif //OSMIORNICASHOOTER_EXCEPTION_H
