#ifndef OSMIORNICASHOOTER_RESOURCEMANAGEREXCEPTION_H
#define OSMIORNICASHOOTER_RESOURCEMANAGEREXCEPTION_H

#include "Exception.h"

namespace ex {

    class ResourceManagerException: public Exception {
    public:
        ResourceManagerException(): Exception("ResourceManagerException") {}
        ResourceManagerException(const std::string& msg): Exception("ResourceManagerException", msg) {};
        ResourceManagerException(const std::exception& original_ex, const std::string& msg = ""): Exception("ResourceManagerException", original_ex, msg) {}
        ResourceManagerException(const Exception& exception): Exception(exception) { cause = "ResourceManagerException->" + cause; }
        ResourceManagerException(const ErrorType& errorType): Exception(errorDescriptions.at(errorType)) {};
        ResourceManagerException(const ErrorType& errorType, const std::string& info): Exception(errorDescriptions.at(errorType), info) {}
    };

}

#endif //OSMIORNICASHOOTER_RESOURCEMANAGEREXCEPTION_H
