#ifndef OSMIORNICASHOOTER_OUTPUT_H
#define OSMIORNICASHOOTER_OUTPUT_H

#include <string>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

class Input;

class Window {
    friend class Input;
public:
    Window(std::string app_version);
    void destroy() const noexcept;

    void draw() const noexcept;
    void getGeometry(int* width, int* height) const noexcept;
    bool isOpened() const noexcept;

    static void errorCallback(int error, const char* description);
private:
    GLFWwindow* glfwWindow;
};

#endif //OSMIORNICASHOOTER_OUTPUT_H
