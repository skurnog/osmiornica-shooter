#ifndef OSMIORNICASHOOTER_INPUT_H
#define OSMIORNICASHOOTER_INPUT_H

#include <io/Window.h>
#include <io/ArgumentsParser.h>
#include <logic/GameEngine.h>
#include <logger/loggers.h>
#include <exceptions/IOException.h>

class Input {
public:
    static void init(Window *window, GameEngine *gameEngine, const ArgumentsParser& programArguments) {
        Input::gameEngine = gameEngine;

        memset(keys, 0, 1024);
        glfwSetKeyCallback(window->glfwWindow, &Input::keyCallback);

        if(!programArguments.isDebugMode()) {
            glfwSetInputMode(window->glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
        glfwSetMouseButtonCallback(window->glfwWindow, &Input::mouseButtonCallback);
        glfwSetCursorPosCallback(window->glfwWindow, &Input::mousePositionCallback);
    }

    static void processEvents() {
        glfwPollEvents();
        if (gameEngine) {
            gameEngine->processKeyEvents(keys);
            gameEngine->processMouseMoveEvents(mouseDeltaX, mouseDeltaY);

            mouseDeltaX = 0;
            mouseDeltaY = 0;
        } else {
            throw ex::IOException("Game engine invalid");
        }
    }
private:
    static GameEngine *gameEngine;

    static bool keys[1024]; // key state array

    static bool firstMouse;
    static double mouseDeltaX;
    static double mouseDeltaY;
    static double mousePositionX;
    static double mousePositionY;

    static int state_lmb;

    static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) keys[key] = true;
        else if (action == GLFW_RELEASE) keys[key] = false;
    }

    static void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods) {
        if (gameEngine) {
            switch (button) {
                case GLFW_MOUSE_BUTTON_RIGHT:
                    if (Input::state_lmb != action) {
                        Input::state_lmb = (action == GLFW_PRESS ? GLFW_PRESS : GLFW_RELEASE);
                        gameEngine->mouseRightButton(action == GLFW_PRESS);
                    }
                    break;
                default:
                    return;
            }
        } else {
            LOG_ERROR(ErrorType::GAME_ENGINE_INVALID) << "mouseButtonCallback";
        }
    }

    static void mousePositionCallback(GLFWwindow *window, double xpos, double ypos) {
        if(firstMouse) {
            mousePositionX = xpos;
            mouseDeltaY = ypos;
            firstMouse = false;
        }

        mouseDeltaX += mousePositionX - xpos;
        mouseDeltaY += mousePositionY - ypos;
        mousePositionX = xpos;
        mousePositionY = ypos;
    }
};

GameEngine *Input::gameEngine = 0;
double Input::mousePositionX = 0;
double Input::mousePositionY = 0;
double Input::mouseDeltaX = 0;
double Input::mouseDeltaY = 0;
bool Input::firstMouse = true;
int Input::state_lmb = -1;
bool Input::keys[1024] = {0};

#endif //OSMIORNICASHOOTER_INPUT_H
