#include "Window.h"

#include <logger/loggers.h>
#include <exceptions/IOException.h>

Window::Window(std::string app_version) {
    if(!glfwInit()) {
        throw ex::IOException(std::runtime_error("glfw cannot be initialized"));
    }
    glfwSetErrorCallback(&Window::errorCallback);

    glfwWindow = glfwCreateWindow(640, 480, ("OSH v" + std::string(app_version)).c_str(), nullptr, nullptr);
    if(!glfwWindow) {
        glfwTerminate();
        throw ex::IOException(std::runtime_error("glfw window cannot be created"));
    }

    glfwMakeContextCurrent(glfwWindow);
    glfwSwapInterval(1);
}

void Window::destroy() const noexcept {
    glfwDestroyWindow(glfwWindow);
    glfwTerminate();
}

void Window::getGeometry(int* width, int* height) const noexcept {
    glfwGetFramebufferSize(glfwWindow, width, height);
}

bool Window::isOpened() const noexcept {
    return !glfwWindowShouldClose(glfwWindow);
}

void Window::draw() const noexcept {
    glfwSwapBuffers(glfwWindow);
}

void Window::errorCallback(int error, const char* description) {
    std::stringstream ss;
    ss << "0x000" << std::hex << error << ", \"" << description << "\"";
    throw ex::IOException(ErrorType::GLFW, description);
}