#ifndef OSMIORNICASHOOTER_ARGUMENTPARSER_H
#define OSMIORNICASHOOTER_ARGUMENTPARSER_H

#include <string>
#include <map>
#include <algorithm>

enum class CmdArgument {
    DEBUG
};

class ArgumentsParser {
public:
    ArgumentsParser(int argc, char **argv);

    bool exists(CmdArgument argument) const noexcept;
    bool isDebugMode() const noexcept;
private:
    char **begin;
    char **end;

    std::map<CmdArgument, std::string> argumentNames = {
            {CmdArgument::DEBUG, "-debug"}
    };
};

#endif //OSMIORNICASHOOTER_ARGUMENTPARSER_H
