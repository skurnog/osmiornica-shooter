#include "ArgumentsParser.h"

ArgumentsParser::ArgumentsParser(int argc, char **argv) {
    begin = argv;
    end = argv + argc;
}

bool ArgumentsParser::exists(CmdArgument argument) const noexcept {
    std::string option = argumentNames.at(argument);

    return std::find_if(begin, end, [option](const std::string &value) {
        return value.compare(option) == 0;
    }) != end;
}

bool ArgumentsParser::isDebugMode() const noexcept {
    return exists(CmdArgument::DEBUG);
}