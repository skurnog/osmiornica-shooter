#version 130

uniform sampler2D textureSampler;
varying lowp vec2 TexCord;

varying lowp vec3 NormalVecAttr;
varying lowp vec3 FragPos;
uniform vec3 lightPos;

uniform float ambientIntensity;

void main() {
    vec3 norm = normalize(NormalVecAttr);
    vec3 lightDir = normalize(lightPos - vec3(1.5f, 1.5f, 1.5f) - FragPos);
    float diffuseIntensity = max(dot(norm, lightDir), 0.0);
    gl_FragColor = texture2D(textureSampler, TexCord) * (ambientIntensity + diffuseIntensity);
}