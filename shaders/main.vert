#version 130

attribute vec3 posAttr;
attribute lowp vec2 texCoord;
attribute lowp vec3 normalVecAttr;

varying lowp vec2 TexCord;
varying lowp vec3 NormalVecAttr;
varying lowp vec3 FragPos;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

uniform vec3 lightPos;

void main() {
    gl_Position = projectionMat * viewMat * modelMat * vec4(posAttr, 1.0);
    TexCord = texCoord;
    NormalVecAttr = normalVecAttr;
    FragPos = vec3(modelMat * vec4(posAttr, 1.0f));
}