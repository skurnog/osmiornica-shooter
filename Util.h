#ifndef OSMIORNICASHOOTER_UTIL_H
#define OSMIORNICASHOOTER_UTIL_H

#include <vector>
#include <random>
#include <sstream>

class Util {
public:
    static std::vector<float> generate_uniform_vector(unsigned count) {
        std::random_device random_device;
        std::mt19937 generator(random_device());
        std::uniform_real_distribution<> distribution(0, 1);

        std::vector<float> result;
        for (int i = 0; i < count; i++) {
            result.push_back(distribution(generator));
        }

        return result;
    }

    static std::vector<std::string> split_string(const std::string& input_string, char delimiter = ' ') {
        std::stringstream ss;
        ss.str(input_string);

        std::vector<std::string> result;

        std::string item;
        while (std::getline(ss, item, delimiter)) {
            if(!item.empty()) result.push_back(item);
        }

        return result;
    }

    static std::string file_name_without_extension(const std::string& file_name) {
        std::vector<std::string> dirs = Util::split_string(file_name, '/');
        std::string name_with_ex = dirs.back();
        std::vector<std::string> name_spitted = split_string(name_with_ex, '.');

        return name_spitted.at(0);
    }
};

#endif //OSMIORNICASHOOTER_UTIL_H
