#include "Osh.h"

#include <graphics/GraphicsEngine.h>
#include <io/ArgumentsParser.h>
#include <io/Input.h>
#include <exceptions/Exception.h>

int main(int argc, char* argv[]) {
    try {
        ArgumentsParser programArguments(argc, argv);

        GameEngine gameEngine;

        Window window(std::string(APP_VERSION));
        GraphicsEngine graphicsEngine(&window);
        gameEngine.setScene(graphicsEngine.getScene());

        Input::init(&window, &gameEngine, programArguments);

        while (window.isOpened()) {
            graphicsEngine.nextFrame();
            Input::processEvents();
            gameEngine.update();
        }

        window.destroy();
    } catch (ex::Exception& e) {
        LOG_ERROR(ErrorType::DEFAULT) << e.what();
        return 1;
    }

	return 0;
}
